import Vue from 'vue';
import Vuex from 'vuex';
import ApiServices from './services/api-service';
Vue.use(Vuex);
const apiService = new ApiServices();
export default new Vuex.Store({
  state: {
    isLoading: false,
    cities: []
  },
  mutations: {
    SET_LOADING_STATUS(state) {
      state.isLoading = !state.isLoading;
    },
    SET_CITIES(state, cities) {
      state.cities = cities;
    },
  },
  actions: {
    fetchCity({ commit }) {
      commit('SET_LOADING_STATUS');
      apiService.getCities()
        .then(response => {
          console.log("response", response);
          return response.data
        })
        .then(cities => {
          commit('SET_CITIES', cities.areas);
          commit('SET_LOADING_STATUS');
        })
    }
  },
  getters: {
    cities: state => state.cities,
    isloading: state => state.isLoading
  }
});
