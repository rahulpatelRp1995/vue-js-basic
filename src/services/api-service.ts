import axios from 'axios'

export default class ApiServices {
    public apiClient = axios.create({
        baseURL: `http://globalcity-20.appspot.com/api/v1`,
        withCredentials: false, // This is the default
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        }
    });

    public getCities() {
        return this.apiClient.get('/area')
    }
}