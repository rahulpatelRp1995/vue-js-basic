import Vue from 'vue';
import Router from 'vue-router';
import Home from './components/Home/home.vue';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/product/:id',
      name: 'product',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './components/HelloWorld.vue'),
      // beforeEnter: (to, from, next) => {
      //   // ...For guard
      // }
    },
    {
      path: '/cities',
      name: 'cities-list',
      component: () => import(/* webpackChunkName: "about" */ './components/Cities/cities.vue'),
    }

  ],
});
